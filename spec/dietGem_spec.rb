require "spec_helper"

describe DietGem  do
  ## Pruebas que afectan a la Gema. 
  it "Debe existir un número de versión" do
    expect(DietGem::VERSION).not_to be nil
  end
  
  ## Pruebas que afectan a la clase MenuDiet.
  describe MenuDiet do
    
    before :all do

      @menu = MenuDiet.new("ALMUERZO", 786.9, 19, 34, 47, "30 - 35%")
      @plato1 = Plato.new("Macarrones con salsa de tomate y queso parmesano","1 1/2 cucharón", "200")
      @plato2 = Plato.new("Escalope de ternera","1 bistec mediano", "100")
      @plato3 = Plato.new("Ensalada básica con zanahoria rallada","guarnición", "120")
      @plato4 = Plato.new("Mandarina","1 grande", "180")
      @plato5 = Plato.new("Pan de trigo integral","1 rodaja", "20")
      
      @menu.push(@plato1)
      @menu.push(@plato2)
      @menu.push(@plato3)
      @menu.push(@plato4)
      @menu.push(@plato5)
      
      @menu2 = MenuDiet.new("MERIENDA", 313.6, 10, 30, 60, "15%")
      @plato6 = Plato.new("Galletas de leche con chocolate y yogur"," 4 unidades", "46")
      @plato7 = Plato.new("Flan de vainilla sin huevo", "1 unidad", "110")
      
      @menu2.push(@plato6)
      @menu2.push(@plato7)
      
      @menu3 = MenuDiet.new("DESAYUNO", 288.0, 17, 21, 62, "15%")
      @plato8 = Plato.new("Leche desnatada","1 vaso", "200")
      @plato9 = Plato.new("Cacao instant´aneo","c/sopera", "10")
      @plato10 = Plato.new("Cereales de desayuno en hojuelas","1 bol pequeño", "40")
      @plato11 = Plato.new("Almendras laminadas (10 unidades)"," 2 c/soperas", "10")
      
      @menu3.push(@plato8)
      @menu3.push(@plato9)
      @menu3.push(@plato10)
      @menu3.push(@plato11)
      
      @menu4 = MenuDiet.new("MEDIA MAÑANA", 255.5, 7, 24, 69, "10%")
      @plato12 = Plato.new("Cerezas"," 10-12 unidades medianas", "40")
      @plato13 = Plato.new("Galletas bifidus con sésamo", " 4 unidades", "40")
      
      @menu4.push(@plato12)
      @menu4.push(@plato13)
      
      @menu5 = MenuDiet.new("CENA", 561.6, 19, 40, 41, "25-30%")
      @plato14 = Plato.new("Crema de bubango", "2 cucharones", "200")
      @plato15 = Plato.new("Tortilla campesina con espinacas", "1 cuña grande", "150")
      @plato16 = Plato.new("Tomate en dados con atún", "5 a 6 c/soperas", "150")
      @plato17 = Plato.new("Pina natural o en su jugo picada", "5 c/soperas", "120")
      @plato18 = Plato.new("Pan de trigo integral", "1 rodaja", "20")  
      
      @menu5.push(@plato14)
      @menu5.push(@plato15)
      @menu5.push(@plato16)
      @menu5.push(@plato17)
      @menu5.push(@plato18)
  
    end
    
    it "Debe existir un título para el menú." do
      expect(@menu.get_titulo()).not_to be nil
      expect(@menu2.get_titulo()).not_to be nil
    end
      
    it "Debe existir el porcentaje que el menú representa de la ingesta diaria" do
      expect(@menu.get_porcentajeTotal()).not_to be nil
      expect(@menu2.get_porcentajeTotal()).not_to be nil
    end
  
    it "Debe existir un Valor Calórico Total." do
      expect(@menu.get_vct()).not_to be nil
      expect(@menu2.get_vct()).not_to be nil
    end
        
    it "Debe existir el porcentaje de proteínas de un conjunto de platos." do
      expect(@menu.get_porcentajeProteinas()).not_to be nil
      expect(@menu2.get_porcentajeProteinas()).not_to be nil
    end
       
    it "Debe existir el porcentaje de grasas de un conjunto de platos." do
      expect(@menu.get_porcentajeGrasas()).not_to be nil
      expect(@menu2.get_porcentajeGrasas()).not_to be nil
    end
      
    it "Debe existir el porcentaje de hidrados de carbono de un conjunto de platos." do
      expect(@menu.get_porcentajeHidratos()).not_to be nil
      expect(@menu2.get_porcentajeHidratos()).not_to be nil
    end

    it "Existe un método para obtener el título del menú." do
      expect(@menu.get_titulo()).to eq("ALMUERZO")
      expect(@menu2.get_titulo()).to eq("MERIENDA")
    end
      
    it "Existe un método para obtener el porcentaje de la ingesta diaria." do
      expect(@menu.get_porcentajeTotal()).to eq("30 - 35%")
      expect(@menu2.get_porcentajeTotal()).to eq("15%")
    end
    
    it "Existe un método para obtener el V.C.T." do
      expect(@menu.get_vct()).to eq(786.9)
      expect(@menu2.get_vct()).to eq(313.6)
    end
      
    it "Existe un método para obtener el porcentaje de proteínas de un conjunto de platos." do
      expect(@menu.porcentajeProteinas()).to eq(19)
      expect(@menu2.porcentajeProteinas()).to eq(10)
    end
    
    it "Existe un método para obtener el porcentaje de grasas de un conjunto de platos." do
      expect(@menu.get_porcentajeGrasas()).to eq(34)
      expect(@menu2.get_porcentajeGrasas()).to eq(30)
    end
      
    it "Existe un método para obtener el porcentaje de hidratos de carbono de un conjunto de platos." do
      expect(@menu.get_porcentajeHidratos()).to eq(47)
      expect(@menu2.get_porcentajeHidratos()).to eq(60)
    end

    it "Existe un método para obtener el menú formateado" do
      expect(@menu.to_s()).not_to be nil
      expect(@menu2.to_s()).not_to be nil
    end
  end
  
  ## Pruebas que afectan a la clase MenuPorAlimentos derivada de la clase MenuDiet.
  describe MenuPorAlimentos do
    
    before :all do
      @ma = MenuPorAlimentos.new("ALMUERZO", 313.6, 10, 30, 60, "15%", "Verduras y otras hortalizas")
      @plato_ma = Plato.new("Ensalada de col y pepino aliñada con ajo y limón", "2 cucharones", "200")
      @ma.push(@plato_ma)
      
      @menu = MenuDiet.new("ALMUERZO", 313.6, 10, 30, 60, "15%")
      @menu.push(@plato_ma)
    end
    
	  it "Objetos creados correctamente" do
	    expect(@plato_ma).to be_an_instance_of Plato
			expect(@ma).to be_an_instance_of MenuPorAlimentos
			expect(@menu).to be_an_instance_of MenuDiet
		end
		
		it "Debe ser una clase derivada de MenuDiet" do
		  expect(@ma).to be_kind_of MenuDiet
		end
		
		it "Debe tener un método to_s" do
		  expect(@ma.to_s).not_to be(NoMethodError)
		end
		
		it "El método to_s debe tener una salida" do
		  expect(@ma.to_s()).not_to be nil
		end
		
		it "El método to_s corresponde al de la clase MenuPorAlimentos y no al de la clase MenuDiet" do
		  expect(@ma.to_s).not_to eq(@menu.to_s)
		end
	end
	
	  ## Pruebas que afectan a la clase MenuPorEdades derivada de la clase MenuDiet.
  describe MenuPorAlimentos do
    
    before :all do
      @me = MenuPorEdades.new("ALMUERZO", 313.6, 10, 30, 60, "15%", "9 a 13 años")
      @plato_me = Plato.new("Ensalada de col y pepino aliñada con ajo y limón", "2 cucharones", "200")
      @me.push(@plato_me)
      
      @menu = MenuDiet.new("ALMUERZO", 313.6, 10, 30, 60, "15%")
      @menu.push(@plato_me)
    end
    
	  it "Objetos creados correctamente" do
	    expect(@plato_me).to be_an_instance_of Plato
			expect(@me).to be_an_instance_of MenuPorEdades
			expect(@menu).to be_an_instance_of MenuDiet
		end
		
		it "Debe ser una clase derivada de MenuDiet" do
		  expect(@me).to be_kind_of MenuDiet
		end
		
		it "Debe tener un método to_s" do
		  expect(@me.to_s).not_to be(NoMethodError)
		end
		
		it "El método to_s debe tener una salida" do
		  expect(@me.to_s()).not_to be nil
		end
		
		it "El método to_s corresponde al de la clase MenuPorEdades y no al de la clase MenuDiet" do
		  expect(@me.to_s).not_to eq(@menu.to_s)
		end
	end
  
  ## Pruebas que afectan a la clase Plato.
  describe Plato do
  
    before :all do
      @plato1 = Plato.new("Macarrones con salsa de tomate y queso parmesano","1 1/2 cucharón", "200")
      @plato2 = Plato.new("Galletas de leche con chocolate y yogur"," 4 unidades", "46")
    end
    
    it "Debe existir la descripción de un plato." do
      expect(@plato1.get_descripcion()).not_to be nil
      expect(@plato2.get_descripcion()).not_to be nil
    end
    
    it "Debe existir la porcíon recomendada de un plato." do 
      expect(@plato1.get_porcion()).not_to be nil
      expect(@plato2.get_porcion()).not_to be nil
    end
    
    it "Debe existir la ingesta de gramos de un plato" do
      expect(@plato1.get_ingestaDiaria()).not_to be nil
      expect(@plato2.get_ingestaDiaria()).not_to be nil
    end
    
    it "Existe un método para obtener la descripción del plato." do
      expect(@plato1.get_descripcion()).to eq("Macarrones con salsa de tomate y queso parmesano")
      expect(@plato2.get_descripcion()).to eq("Galletas de leche con chocolate y yogur")
    end
    
    it "Existe un método para obtener la porcíon recomendada de un plato." do 
      expect(@plato1.get_porcion()).to eq("1 1/2 cucharón")
      expect(@plato2.get_porcion()).to eq(" 4 unidades")
    end
    
    it "Existeun metodo para obtener la ingesta de gramos de un plato" do
      expect(@plato1.get_ingestaDiaria()).to eq("200")
      expect(@plato2.get_ingestaDiaria()).to eq("46")
    end 
  end

  ## Pruebas que afectan a la clase o estructura Node.
  describe Node do	
    before :all do
     @nn = Node.new(2)
     @np = Node.new(0)
     @n1 = Node.new(@np, 1, @nn)
    end
    
    it "Objetos creados correctamente" do
      expect(@n1).to be_an_instance_of Node
      expect(@nn).to be_an_instance_of Node
      expect(@np).to be_an_instance_of Node
    end
    
    it "Debe poder acceder a su valor" do
      expect(@n1[:value]).to eq(1)
    end
    
    it "Debe apuntar al siguiente nodo" do
      expect(@n1[:next]).to eq(@nn)
    end
    
    it "Debe apuntar al nodo anterior" do
      expect(@n1[:prev]).to eq(@np)
    end
    
	end

  ## Pruebas que afectan a la clase LinkedList.
	describe LinkedList do
	  before :all do
	    @n1 = Node.new(nil, 1, nil)
	    @n2 = Node.new(nil, 2, Node.new(nil, 3, Node.new(nil, 1)))
	    
	    @list = LinkedList.new(0)
	  end
    
    it "Objetos creados correctamente" do
      expect(@list).to be_an_instance_of LinkedList
      
      expect(@n1).to be_an_instance_of Node
      expect(@n2).to be_an_instance_of Node
    end
      
    it "Debe existir un head" do
			expect(@list.head).to be_an_instance_of Node
		end
    
		it "Se debe poder extraer un elemento de la lista" do
			expect(@list.pop_front).to eq(0)
		end

		it "Se debe poder insertar un elemento" do
			expect(@list.insert(0, 1).head).to eq(@n1)
		end

		it "Se debe poder insertar varios elementos" do
			expect(@list.insert(0,2,3).head).to eq(@n2)
		end
		
		it "Existe un método to_s" do
		  expect(@list.to_s).not_to be(NoMethodError)
		end
	end
	
end


  