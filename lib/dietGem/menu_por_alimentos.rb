require_relative "menu"

class MenuPorAlimentos < MenuDiet
    attr_accessor :tipo
    
    def initialize(titulo, vct, pProteinas, pGrasas, pHidratos, pTotal, tipo)
        super(titulo, vct, pProteinas, pGrasas, pHidratos, pTotal)
        @tipo = tipo
    end

    def to_s()
       output = "TIPO DE ALIMENTOS: #{@tipo}\n\n"
       output << "#{@titulo}\n"
       output << "(#{@porcentajeTotal})\n "      
       
       it = 0
       while(it < get_totalPlatos())
            output << "#{@platos[it].to_s()}"
            it = it + 1
       end
        
        output << "V.C.T  | %, #{@vct} | #{@porcentajeProteinas}% - #{@porcentajeGrasas}% - #{@porcentajeHidratos}"
        
        output
    end
end