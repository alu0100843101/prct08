## En la prct08 añadimos que el nodo tendrá un prev y un next. 
Node = Struct.new(:prev, :value, :next)

class LinkedList
	attr_reader :head

    #Al crear la lista, si no le damos ningún valor, tomaremos por defecto un valor nulo (nil).
	def initialize(value = nil) 
		## En la prct08 añadimos que el nodo tendrá un prev y un next. 
		@head = Node.new(nil, value, nil)
	end

    #Insertamos un valor (value) en la última posición (size).
	def push_back value
		insert(size, value)
	end

    #Insertamos un valor (value) en la primera posición (0).
	def push_front value
		insert(0, value)
	end

    #Extraemos un valor (value) de la última posición (size), eliminando este último nodo y haciendo que el último sea el penúltimo (current).
	def pop_back
		current = @head
		for i in 1..size-1
			current = current[:next]
		end
		aux = current[:next][:value]
		erase(size)
		return aux
	end

    #Extraemos un valor (value) de la primera posición (0), eliminando este nodo.
	def pop_front
		aux = @head[:value]
		erase(0)
		return aux
	end

    #Insertar en una posición concreta de la lista un valor. Tanto si la posición es la primera como si no. 
	def insert(position, *values)
		current = @head
		if position == 0
			for value in values.reverse
				## En la prct08 añadimos que el nodo tendrá un prev y un next. 
				@head = Node.new(nil, value, @head)
			end
		else
			for i in 1..position-1
				current = current[:next]
			end
			for value in values.reverse
				## En la prct08 añadimos que el nodo tendrá un prev y un next. 
				current[:next] = Node.new(current, value, current[:next])
			end
		end
		self
	end

    #Eliminar un nodo. 
	def erase(position)
		if position > size
			nil
		end
		if position == 0
			if @head[:next].nil?
				@head = nil
			else
				@head = @head[:next]
			end
		else
			current = @head
			for i in 1..position-1
				current = current[:next]
			end
			current[:next] = (current[:next])[:next]
		end
	end	

    #Calcular el tamaño de la lista. 
	def size
		if @head == nil
			0
		end
		counter = 1
		current = @head
		until current[:next] == nil
			counter+=1
			current=current[:next]
		end
		counter
	end

    #Acceder a una posición de la lista - Sobrecarga de operador. 
	def [](position)
			if position > size
				return nil
			end
			current = @head
			for i in 1..position-1
				current = current[:next]
			end
			return current[:next][:value]
	end

    #Mostrar los elementos de la lista separados por comas (funcion join). 
	def to_s
		current = @head
		to_s_array = []
		until current[:next] == nil
			to_s_array.push(current[:value])
			current=current[:next]
		end
		to_s_array.push(current[:value])
		"[#{to_s_array.join(', ')}]"
	end

end