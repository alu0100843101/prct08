require_relative "menu"

class MenuPorEdades < MenuDiet
    attr_accessor :edades
    
    def initialize(titulo, vct, pProteinas, pGrasas, pHidratos, pTotal, edades)
        super(titulo, vct, pProteinas, pGrasas, pHidratos, pTotal)
        @edades = edades
    end

    def to_s()
       output = "EDADES RECOMENDADAS: #{@edades}\n\n"
       output << "#{@titulo}\n"
       output << "(#{@porcentajeTotal})\n "      
       
       it = 0
       while(it < get_totalPlatos())
            output << "#{@platos[it].to_s()}"
            it = it + 1
       end
        
        output << "V.C.T  | %, #{@vct} | #{@porcentajeProteinas}% - #{@porcentajeGrasas}% - #{@porcentajeHidratos}"
        
        output
    end
end