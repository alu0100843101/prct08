class MenuDiet

 attr_reader :titulo, :platos, :vct, :porcentajeProteinas, :porcentajeGrasas, :porcentajeHidratos, :porcentajeTotal
    
    def initialize(titulo, vct, porcentajeProteinas, porcentajeGrasas, porcentajeHidratos, porcentajeTotal)
       @titulo, @vct, @porcentajeProteinas, @porcentajeGrasas, @porcentajeHidratos, @porcentajeTotal = titulo, vct , porcentajeProteinas, porcentajeGrasas, porcentajeHidratos, porcentajeTotal
       @platos = []
       @totalPlatos = 0
    end
    
    def get_porcentajeTotal()
        @porcentajeTotal
    end
    
    def get_totalPlatos()
        @totalPlatos
    end
    
    def push(plato)
        @platos.push(plato)
        @totalPlatos = @totalPlatos + 1
    end 
    
    def get_titulo()
        @titulo
    end

    def get_vct()
        @vct
    end
    
    def get_porcentajeProteinas()
        @porcentajeProteinas
    end
    
    def get_porcentajeGrasas()
        @porcentajeGrasas
    end
    
    def get_porcentajeHidratos()
        @porcentajeHidratos
    end
    
    def get_platos()
        it = 0
        while(it < get_totalPlatos())
            @platos[it]
            it = it + 1
        end
    end
    
    def to_s()
       output = @titulo
       output << "(#{@porcentajeTotal})\n "      
       
       it = 0
       while(it < get_totalPlatos())
            output << "#{@platos[it].to_s()}"
            it = it + 1
       end
        
        output << "V.C.T  | %, #{@vct} | #{@porcentajeProteinas}% - #{@porcentajeGrasas}% - #{@porcentajeHidratos}"
        
        output
    end
    
end 

class Plato

  attr_reader :ingestaDiaria, :descripcion, :porcion
   
    def initialize(descripcion, porcion, ingestaDiaria)
       @ingestaDiaria, @descripcion, @porcion = ingestaDiaria, descripcion, porcion
    end
    
    def get_ingestaDiaria()
        @ingestaDiaria
    end
    
    def get_descripcion()
        @descripcion
    end
    
    def get_porcion()
        @porcion
    end
    
    def to_s()
       "- #{@descripcion}, #{@porcion}, #{@ingestaDiaria} gr"
    end
end
  


  
